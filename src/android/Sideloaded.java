package com.creativevikings.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class Sideloaded extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("downloadStore")) {
            this.downloadStore(callbackContext);
            return true;
        }
        return false;
    }

    private void downloadStore(CallbackContext callbackContext) {
        callbackContext.success(this.cordova.getActivity().getPackageManager().getInstallerPackageName(this.cordova.getActivity().getPackageName()));
    }
}
