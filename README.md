# README #

A Cordova plugin that checks if an android app was side loaded or downloaded from the play store.

### Supported Platforms ###
* Android

### How do I get set up? ###

* Add the dependency to you Cordova project. E.g. by adding the following to your config.xml file
`<plugin name="cordova-plugin-sideloaded"/>`

### How to use this plugin ###
This plugin is easy to use, it has just one method which tells you from where to app was downloaded.
Internally it uses Android's [PackageManager]([https://developer.android.com/reference/android/content/pm/PackageManager.html#getInstallerPackageName%28java.lang.String%29).

#### Example ####
`cordova.plugins.Sideloaded.getDownloadStore((res)=>{console.log(res)})`

Possible return values
* null if it was sideloaded
* string if it was downloaded form a store (e.g. 'com.android.vending' for Google's Play Store)

### Who do I talk to? ###

* You can contact me at contact@creativevikings.com