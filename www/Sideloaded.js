var exec = require('cordova/exec');

var PLUGIN_NAME = "downloadStore";

var MyCordovaPlugin = {
    getDownloadStore: function (callBack) {
        exec(callBack, null, "Sideloaded", "downloadStore", []);
    }
};

module.exports = MyCordovaPlugin;